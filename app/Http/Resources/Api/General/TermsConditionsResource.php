<?php

namespace App\Http\Resources\Api\General;

use Illuminate\Http\Resources\Json\JsonResource;

class TermsConditionsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'content_ar'  => $this->translate('ar')->content,
            'content_en'  => $this->translate('en')->content,
        ];
    }
}
