<?php

namespace App\Http\Resources\Api\Resturant_App\Resturant;

use Illuminate\Http\Resources\Json\JsonResource;

class DayResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'day_ar'       => $this->translate('ar')->name,
            'day_en'       => $this->translate('en')->name,
        ];
    }
}
