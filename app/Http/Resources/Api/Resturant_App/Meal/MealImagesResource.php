<?php

namespace App\Http\Resources\Api\Resturant_App\Meal;

use Illuminate\Http\Resources\Json\JsonResource;

class MealImagesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'    => $this->id,
            'image' => $this->image
        ];
    }
}
