<?php

namespace App\Http\Resources\Api\Resturant_App\Meal;

use App\Http\Resources\Api\Resturant_App\Extra\ExtraResource;
use Illuminate\Http\Resources\Json\JsonResource;

class MealResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->id,
            'resturant_id'        => $this->resturant_id,
            'name_ar'             => $this->translate('ar')->name,
            'name_en'             => $this->translate('en')->name,
            'price'               => $this->price,
            'total_calories'      => $this->total_calories,
            'carbohydrate'        => $this->carbohydrate,
            'protein'             => $this->protein,
            'fats'                => $this->fats,
            'main_image'          => $this->main_image,
            'status'              => $this->status,
            'description_ar'      => $this->translate('ar')->description,
            'description_en'      => $this->translate('en')->description,
            'discount'            => $this->discount ? new MealDiscountResource($this->discount) : null,
            'images'              => $this->images ?  MealImagesResource::collection($this->images) : null,
            'extras'              => $this->extras ? ExtraResource::collection($this->extras) : null,
        ];
    }
}
