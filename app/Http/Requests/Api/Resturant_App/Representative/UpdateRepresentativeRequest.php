<?php

namespace App\Http\Requests\Api\Resturant_App\Representative;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRepresentativeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                            => 'required|string',
            'email'                           => 'required|email',
            'mobile'                          => 'required|string',
            'city_id'                         => 'required|integer',
            'personal_image'                  => 'required|image|mimes:jpg,jpeg,png,gif,webp',
            'vehicle_type'                    => 'required|string',
            'vehicle_brand'                   => 'required|string',
            'type'                            => 'required|string',
            'vehicle_model'                   => 'required|string',
            'license_image'                   => 'required|image|mimes:jpg,jpeg,png,gif,webp',
            'form_image'                      => 'required|image|mimes:jpg,jpeg,png,gif,webp',
        ];
    }
}
