<?php

namespace App\Http\Requests\Api\Resturant_App\Resturant;

use Illuminate\Foundation\Http\FormRequest;

class StoreResturantWorkingHoursRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // The 4 Satage Data
            'working_days.*.day_id'           => 'required|integer',
            'working_days.*.opening_hour'     => 'required',
            'working_days.*.closing_hour'     => 'required',
        ];
    }
}
