<?php

namespace App\Http\Requests\Api\Resturant_App\Resturant;

use Illuminate\Foundation\Http\FormRequest;

class StoreResturantDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // The 2 Satage Data
            'description_ar'                  => 'required|string',
            'description_en'                  => 'required|string',
            'city_id'                         => 'required|integer',
            'latitude'                        => 'required',
            'longitude'                       => 'required',
            'logo'                            => 'required|image|mimes:jpg,jpeg,png,gif,webp',
            'main_image'                      => 'required|image|mimes:jpg,jpeg,png,gif,webp',
            'images'                          => 'required|array',
            'images.*'                        => 'required|image|mimes:jpg,jpeg,png,gif,webp',
        ];
    }
}
