<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MealDiscount extends Model
{
    use HasFactory;

    protected $fillable = ['meal_id', 'has_discount', 'discount_type', 'discount_value', 'discount_start_date', 'discount_end_date'];
    
    ##---------- Relationships ----------##

    /**
     * Get the meal that owns the discount.
     */
    public function meal()
    {
        return $this->belongsTo(Meal::class);
    }
}
