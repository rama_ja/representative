<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MealImage extends Model
{
    use HasFactory;

    protected $fillable = ['meal_id', 'image'];

    ##---------- Relationships ----------##

    /**
     * Get the meal that owns the image.
     */
    public function meal()
    {
        return $this->belongsTo(Meal::class);
    }
}
