<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Extra extends Model implements TranslatableContract
{
    use HasFactory, Translatable;

    protected $fillable = ['resturant_id', 'price', 'image'];

    public $translatedAttributes = ['name', 'description'];

    ##---------- Relationships ----------##

    /**
     * The meals that belong to the extra.
     */
    public function meals()
    {
        return $this->belongsToMany(Meal::class);
    }
}
